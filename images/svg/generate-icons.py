#! /usr/bin/python

import os
import re
import tempfile
import xml.dom.minidom

backgrounds = [
    "square",
    "circle",
    "hexagon",
    "triangle",
]
foregrounds = [
    "melee",
    "ranged",
    "close_blast",
    "close_burst",
    "area_burst",
    "other",
]
colors = [
    ("orange", "#f9dd87", "#f2ba0e"),
    ("green", "#b9e49c", "#71c837"),
    ("blue", "#81b4ff", "#0166ff"),
    ("red", "#ff8181", "#ff2a2a"),
]

extra_icons = ['default', 'disabled', 'folder', 'character', 'dice']

def icon_name(bg, fg, color):
    # Orange icons are generic melee/ranged icons.
    if color == "orange":
        if bg == "square" and (fg == "melee" or fg == "ranged"):
            return fg
        else:
            return None
    return "dnd_4e_%s_%s_%s" % (fg, bg, color)

def svg_to_png(svg, png):
    os.spawnvp(os.P_WAIT, "inkscape",  ("-C", "-e", png, svg))

for bg in backgrounds:
    bg_doc = xml.dom.minidom.parse("svg/tree_bg_%s.svg" % bg)
    bg_group = bg_doc.getElementsByTagName("g")[0]
    bg_path = bg_doc.getElementsByTagName("path")[0]
    bg_style = bg_path.getAttribute("style")

    for fg in foregrounds:
        fg_doc = xml.dom.minidom.parse("svg/tree_fg_dnd_4e_%s.svg" % fg)
        fg_group = fg_doc.getElementsByTagName("g")[0]

        bg_group.parentNode.appendChild(fg_group)

        for (color, fill, stroke) in colors:
            name = icon_name(bg, fg, color)
            if not name:
                continue

            style = re.sub("fill:#......", "fill:%s" % fill, bg_style)
            style = re.sub("stroke:#......", "stroke:%s" % stroke, style)
            bg_path.setAttribute("style", style)

            f = tempfile.NamedTemporaryFile(suffix=".svg")
            f.file.write(bg_doc.toxml())
            f.file.flush()

            svg_to_png(f.name, "tree_%s.png" % name)

            f.close()

        bg_group.parentNode.removeChild(fg_group)

f = open("icons.xml", "wb")
f.write("<icons>\n")
for icon in extra_icons:
    svg_to_png("svg/tree_%s.svg" % icon, "tree_%s.png" % icon)
    f.write("  <icon name='%s' file='tree_%s.png'/>\n" % (icon, icon))
for (color, fill, stroke) in colors:
    for bg in backgrounds:
        for fg in foregrounds:
            name = icon_name(bg, fg, color)
            if not name:
                continue
            f.write("  <icon name='%s' file='tree_%s.png'/>" % (name, name))
f.write("</icons>\n")
