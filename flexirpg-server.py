#!/usr/bin/env python
import os
import string
import sys
import time
import gc
import getopt
import traceback

# Simple usuage text for request via help or command line errors
def usage( retValue ):
    print("\nUsage: \n[-d --directory directory] - Directory of where to load config files\n" + \
    "[-n Server Name]\n" + \
    "[-l Lobby Boot Password]\n" + \
    "[-r Run From???]\n" + \
    "[-h --help]\n\n" + \
    "[-m --manual]\n\n" +\
    sys.exit( retValue ))

import orpg.networking.mplay_server

if __name__ == '__main__':
    gc.set_debug(gc.DEBUG_UNCOLLECTABLE)
    gc.enable()

    orpg_server = orpg.networking.mplay_server.mplay_server()
    lobby_boot_pwd = orpg_server.boot_pwd
    server_directory = orpg_server.userPath
    server_name = orpg_server.serverName
    manualStart = False

    # See if we have command line arguments in need of processing
    try:
        (opts, args) = getopt.getopt( sys.argv[1:], "n:phml:m:d:", ["help","manual","directory="] )
        for o, a in opts:
            if o in ("-d", "--directory"):
                if (a[(len(a)-1):len(a)] != os.sep):
                    a = a + os.sep
                    if not (os.access(a, os.W_OK) and os.path.isdir(a)):
                        print("*** ERROR *** Directory '" + a + "' Either doesn't exist, or you don't have permission to write to it.")
                        sys.exit(1)
                server_directory = a
            # Server Name
            if o in ( "-n", ):
                server_name = a
            # Lobby Password
            if o in ( "-l", ):
                lobby_boot_pwd = a
            # Help
            if o in ( "-h", "--help" ):
                usage( 0 )
            #Dont Auto Init Server
            if o in ("-m", "--manual"):
                manualStart = True
    except:
        print()
        usage( 1 )


    # start server!
    orpg_server.serverName = server_name
    orpg_server.boot_pwd = lobby_boot_pwd
    orpg_server.userPath = server_directory

    if not manualStart:
        orpg_server.initServer()

    print("-----------------------------------------------------")
    print("Type 'help' or '?' or 'h' for server console commands")
    print("-----------------------------------------------------")

    opt = "None"
    orpg_server.console_log()
    try:
        while (opt != "kill") and ( opt != "quit"):
            opt = input("action?:")
            words = opt.split()
            if opt == "initserver":
                userpath = input("Please enter the directory you wish to load files from [myfile]:")
                orpg_server.initServer(userPath=userpath)
            elif opt == "broadcast":
                msg = input("Message:")
                orpg_server.broadcast(msg)
            elif opt == "dump":
                orpg_server.player_dump()
            elif opt == "dump groups":
                orpg_server.groups_list()
            elif opt == "get lobby boot password":
                print("Lobby boot password is:  " + orpg_server.groups['0'].boot_pwd)
                print()
            elif opt == "set lobby boot password":
                lobby_boot_pwd = input("Enter boot password for the Lobby:  ")
                orpg_server.groups['0'].boot_pwd = lobby_boot_pwd
            elif len(words) == 2 and words[0] == "group":
                orpg_server.group_dump(words[1])
            elif opt == "help" or opt == "?" or opt == "h":
                orpg_server.print_help()
            elif opt == "remove room":
                print("Removing a room will kick everyone in that room off your server.")
                print("You might consider going to that room and letting them know what you are about to do.")
                groupnumber = input("Room group number:")
                orpg_server.remove_room(groupnumber)
            elif opt == "roompasswords":
                print(orpg_server.RoomPasswords())
            elif opt == "list":
                orpg_server.player_list()
            elif opt == "log":
                orpg_server.console_log()
            elif opt == "kick":
                kick_id = input("Kick Player #  ")
                kick_msg = input("Reason(optional):  ")
                orpg_server.admin_kick(kick_id,kick_msg)
            elif opt == "sendsize":
                send_len = input("Send Size #  ")
                orpg_server.admin_setSendSize(send_len)
            else:
                if (opt == "kill") or (opt == "quit"):
                    print ("Closing down server. Please wait...")
                else:
                    print(("[UNKNOWN COMMAND: \""+opt+"\" ]"))

    except Exception as e:
        print("EXCEPTION: "+str(e))
        traceback.print_exc()
        input("press <enter> key to terminate program")

    orpg_server.kill_server()
