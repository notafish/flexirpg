#
# FlexiRPG Makefile
#
# Copyright (C) 2009 David Vrabel
#

PACKAGE := flexirpg
VERSION := $(shell python orpg/orpg_version.py)

all: icons ply-tables

dir := $(PACKAGE)-$(VERSION)
zip := $(dir).zip

icons:
	make -C images

ply-tables:
	python -m orpg.dieroller.parser

dist:
	@if [ -e $(zip) ]; then echo "Error: $(zip) already exists"; exit 1; fi
	git archive --prefix=$(dir)/ HEAD | tar xf -
	$(MAKE) -C $(dir) all
	zip -r $(zip) $(dir)
	rm -r $(dir)
