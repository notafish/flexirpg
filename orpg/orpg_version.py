# Product name and version.
#
# Copyright (C) 2011 David Vrabel
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

PRODUCT = "FlexiRPG"

VERSION_MAJOR = 1
VERSION_MINOR = 96
VERSION_MICRO = 3
VERSION_EXTRA = ""

SERVER_MIN_CLIENT_VERSION = "1.96.1"

# This version is for network capability.
PROTOCOL_VERSION = "2.2"

VERSION = "%d.%d.%d" % (VERSION_MAJOR, VERSION_MINOR, VERSION_MICRO)

CLIENT_STRING = "%s %s%s" % (PRODUCT, VERSION, VERSION_EXTRA)

if __name__ == "__main__":
    print(VERSION + VERSION_EXTRA)
