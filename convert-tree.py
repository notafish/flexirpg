#! /usr/bin/python
# Convert all game tree nodes to flexinodes.

import distutils.core
import distutils.file_util as file_util
import errno
import os
import re
import sys
import xml.dom.minidom

import orpg.dirpath
import orpg.orpg_xml

class converter(object):
    def __init__(self, doc):
        self.doc = doc
        self.errors = {}
        self.warnings = {}
        pass

    def convert(self):
        for nodehandler in self.doc.getElementsByTagName('nodehandler'):
            cls = nodehandler.getAttribute('class')
            try:
                func = getattr(self, 'convert_' + cls)
            except:
                self.error("cannot convert '%s' node" % (cls))
                continue
            func(nodehandler)

    def convert_flexinode(self, node):
        if node.hasAttribute('version'):
            node.removeAttribute('version')
        if node.hasAttribute('module'):
            node.removeAttribute('module')

    def convert_rpg_grid_handler(self, node):
        self.warning("discarding 'rpg_grid_handler' node")
        self.discard(node)

    def convert_textctrl_handler(self,node):
        self.to_flexinode(node, 'dice')
        child = node.firstChild
        if child.tagName == 'text':
            if child.firstChild:
                self.add_action(node, child.firstChild.data)
            node.removeChild(child)

    def convert_form_handler(self, node):
        if node.firstChild.tagName == 'form':
            node.removeChild(node.firstChild)
        self.to_flexinode(node, 'folder')

    def convert_group_handler(self, node):
        if node.firstChild.tagName == 'group_atts':
            node.removeChild(node.firstChild)
        self.to_flexinode(node, 'folder')

    def convert_splitter_handler(self, node):
        if node.firstChild.tagName == 'splitter':
            node.removeChild(node.firstChild)
        self.to_flexinode(node, 'folder')

    def convert_tabber_handler(self, node):
        self.to_flexinode(node, 'folder')

    def convert_listbox_handler(self, node):
        self.to_flexinode(node, 'folder')
        l = node.firstChild
        for opt in l.getElementsByTagName('option'):
            text = opt.firstChild
            if not text:
                continue
            name = text.data
            if len(name) > 20:
                name = name[:20] + '...'
            new = self.new_flexinode(name, 'dice')
            self.add_action(new, text.data)
            node.appendChild(new)
        node.removeChild(l)

    def convert_d20char_handler(self, node):
        self.to_flexinode(node, 'character')
        next = node.firstChild
        while next:
            child = next
            next = child.nextSibling
            try:
                func = getattr(self, "d20_convert_" + child.tagName)
            except:
                self.error("cannot convert d20 '%s' node" % (child.tagName))
                self.discard(child)
                continue
            new_child = func(child)
            if new_child:
                node.insertBefore(new_child, child)
            self.discard(child)

    def d20_convert_howtouse(self, node):
        return None

    def d20_convert_general(self, node):
        self.warning("discarding d20 'general' node")
        return None

    def d20_convert_inventory(self, node):
        self.warning("discarding d20 'inventory' node")
        return None

    def d20_convert_powers(self, node):
        self.warning("discarding d20 'powers' node")
        return None

    def d20_convert_pp(self, node):
        self.warning("discarding d20 'pp' node")
        return None

    def d20_convert_classes(self, node):
        classes = self.new_flexinode('Classes', 'folder')
        for cls in node.getElementsByTagName('class'):
            name = cls.getAttribute('name')
            level = cls.getAttribute('level')

            new = self.new_flexinode("%s (%s)" % (name, level), 'default')
            classes.appendChild(new)
        return classes

    def d20_convert_abilities(self, node):
        abilities = self.new_flexinode('Abilities', 'folder')
        for stat in node.getElementsByTagName('stat'):
            name = stat.getAttribute('name')
            abbr = stat.getAttribute('abbr')
            base = stat.getAttribute('base')

            new = self.new_flexinode(name, 'dice')
            self.add_expr(abilities, name, base)
            self.add_expr(new, abbr, '(%s - 10)/2' % (name))
            self.add_action(new, '[1d20+%s]' % (abbr))
            abilities.appendChild(new)
        return abilities

    def d20_convert_saves(self, node):
        saves = self.new_flexinode('Saves', 'folder')
        for save in node.getElementsByTagName('save'):
            name = save.getAttribute('name')
            base = save.getAttribute('base')
            magmod = save.getAttribute('magmod')
            miscmod = save.getAttribute('miscmod')
            stat = save.getAttribute('stat')

            new = self.new_flexinode(name, 'dice')
            self.add_expr(new, 'base', base)
            self.add_expr(new, 'magmod', magmod)
            self.add_expr(new, 'miscmod', miscmod)
            self.add_action(new, '[1d20+(base + magmod + miscmod + %s)]' % (stat))
            saves.appendChild(new)
        return saves

    def d20_convert_skills(self, node):
        skills = self.new_flexinode('Skills', 'folder')
        for skill in node.getElementsByTagName('skill'):
            ac = skill.getAttribute('armorcheck')
            misc = skill.getAttribute('misc')
            name = skill.getAttribute('name')
            rank = skill.getAttribute('rank')
            stat = skill.getAttribute('stat')
            untr = skill.getAttribute('untrained')

            new = self.new_flexinode(name, 'dice')
            self.add_expr(new, 'rank', rank)
            self.add_expr(new, 'misc', misc)
            self.add_expr(new, 'armorcheck', ac)
            self.add_expr(new, 'untrained', untr)
            self.add_action(new, '[if rank == 0 and not untrained then "Untrained" else "[1d20+(rank+misc+%s+(if armorcheck then acp else 0))]"]' % (stat))
            skills.appendChild(new)
        return skills

    def d20_convert_feats(self, node):
        feats = self.new_flexinode('Feats', 'folder')
        for feat in node.getElementsByTagName('feat'):
            name = feat.getAttribute('name')

            new = self.new_flexinode(name, 'default')
            feats.appendChild(new)
        return feats

    def d20_convert_spells(self, node):
        spells = self.new_flexinode('Arcane Spells', 'folder')
        for spell in node.getElementsByTagName('spell'):
            name = spell.getAttribute('name')

            new = self.new_flexinode(name, 'dice')
            self.add_action(new, "Casts [__name]")
            spells.appendChild(new)
        return spells

    def d20_convert_divine(self, node):
        spells = self.new_flexinode('Divine Spells', 'folder')
        for spell in node.getElementsByTagName('gift'):
            name = spell.getAttribute('name')

            new = self.new_flexinode(name, 'dice')
            self.add_action(new, "Casts [__name]")
            spells.appendChild(new)
        return spells

    def d20_convert_hp(self, node):
        hp = self.new_flexinode('Hitpoints', 'dice')
        self.add_expr(hp, 'max_hp', node.getAttribute('max'))
        self.add_expr(hp, 'cur_hp', node.getAttribute('current'))
        self.add_action(hp, '[cur_hp] / [max_hp]')
        return hp

    def d20_convert_attacks(self, node):
        attacks = self.new_flexinode('Attacks', 'folder')
        self.add_expr(attacks, 'bab', node.firstChild.getAttribute('base'))
        self.add_expr(attacks, '_a', '0')
        self.add_expr(attacks, '_d', '0')

        for weapon in node.getElementsByTagName('weapon'):
            name = weapon.getAttribute('name')
            crit = weapon.getAttribute('critical')
            dam = weapon.getAttribute('damage')
            mod = weapon.getAttribute('mod')

            m = re.match('(([0-9]+)-)?([0-9]+/)?x([0-9]+)', crit)
            if m == None:
                print crit
            crit_low = m.group(2)
            if not crit_low:
                crit_low = '20'
            crit_mult = m.group(4)
            if not crit_mult:
                crit_mult = '1'

            crit_dam = re.sub('[-+][0-9]+d[0-9]+$', '', dam)
            m = re.match('([0-9]+)d([0-9]+)([-+])?([-+*/0-9SD]*)', crit_dam)
            crit_dam = '%sd%s' % (str(int(m.group(1)) * (int(crit_mult)-1)), m.group(2))
            if m.group(3):
                crit_dam += '%s(%d*(%s))' % (m.group(3), int(crit_mult)-1, m.group(4))

            mod = re.sub('^[+]', '', mod)
            mod = re.sub('S', 'Str', mod)
            mod = re.sub('D', 'Dex', mod)
            dam = re.sub('S', 'Str', dam)
            dam = re.sub('D', 'Dex', dam)
            crit_dam = re.sub('S', 'Str', crit_dam)
            crit_dam = re.sub('D', 'Dex', crit_dam)

            new = self.new_flexinode(name, 'dice')
            self.add_action(new,'[(_a=1d20)+(bab+%s)]; <i>Hit:</i> [_d=%s] damage.[if _a >= %s then " <i>Critical threat:<i> [1d20+(bab+%s)]; <i>Hit:</i> [_d+%s] damage." else ""]' % (mod, dam, crit_low, mod, crit_dam))
            attacks.appendChild(new)
        return attacks

    def d20_convert_ac(self, node):
        ac = self.new_flexinode('Armour (None)', 'folder')
        self.add_expr(ac, 'acp', '0')
        self.add_action(ac, 'Removes Armour[#__name="Armour (None)"][#acp=0]')
        for armor in node.getElementsByTagName('armor'):
            name = armor.getAttribute('name')
            cp = armor.getAttribute('checkpenalty')

            new = self.new_flexinode(name, 'dice')
            self.add_expr(new, 'check_penalty', cp)
            self.add_action(new, 'Dons [__name][#__parent_name="Armour ([__name])"][#acp=check_penalty]')
            ac.appendChild(new)
        return ac

    def error(self, msg):
        if msg in self.errors:
            self.errors[msg] += 1
        else:
            self.errors[msg] = 1

    def warning(self, msg):
        if msg in self.warnings:
            self.warnings[msg] += 1
        else:
            self.warnings[msg] = 1

    def discard(self, node):
        node.parentNode.removeChild(node)

    def to_flexinode(self, node, icon):
        node.setAttribute('class', 'flexinode')
        node.setAttribute('icon', icon)

    def new_flexinode(self, name, icon):
        node = self.doc.createElement('nodehandler')
        node.setAttribute('name', name)
        self.to_flexinode(node, icon)
        return node

    def add_expr(self, node, var, val):
        expr_node = self.doc.createElement('expression')
        expr_node.setAttribute('variable', var)
        val_node = self.doc.createTextNode(val)
        expr_node.appendChild(val_node)
        node.appendChild(expr_node)

    def add_action(self, node, text):
        action_node = self.doc.createElement('action')
        text_node = self.doc.createTextNode(text)
        action_node.appendChild(text_node)
        node.appendChild(action_node)

def done(code):
    raw_input("Press Enter to continue.")
    sys.exit(code)

tree_file = orpg.dirpath.dir_struct['user'] + 'tree.xml'
backup_file = orpg.dirpath.dir_struct['user'] + 'tree_backup.xml'

try:
    os.stat(backup_file)
    print "Error: backup file (%s) exists" % (backup_file)
    done(1)
except OSError, e:
    try:
        file_util.copy_file(tree_file, backup_file, preserve_mode=1, preserve_times=1)
    except Exception, e:
        print "Error copying '%s' -> '%s': %s" % (tree_file, backup_file, e.message)
        done(1)

doc = orpg.orpg_xml.parse_file(tree_file)

c = converter(doc)
c.convert()

for err in c.errors.keys():
    print "Error: %s (x%d)" % (err, c.errors[err])

for warn in c.warnings.keys():
    print "Warning: %s (x%d)" % (warn, c.warnings[warn])

if len(c.errors) > 0:
    done(1)

f = open(tree_file, 'w')
f.write(doc.toprettyxml(indent='  '))
f.close()

done(0)
