# FlexiRPG 1.96.3

## Introduction

FlexiRPG is a virtual tabletop for pen and paper role playing games.

It aims to be rule system agnostic by providing flexible tools and
features.

FlexiRPG was originally derived from [OpenRPG](http://www.rpgobjects.com/index.php?c=orpg).

## Requirements

* [Python](http://www.python.org/) (2.7 or later).
* [wxPython](http://www.wxpython.org/) 4.0 or later.
* [PLY](http://www.dabeaz.com/ply/) 3.10 or later.

## Installing dependencies.

### pip

    sudo apt-get install python-pip

### PLY

    pip install --user -U ply

### wxPython

Install a pre-built package for your OS or Linux distribution.

If there is no suitable pre-built package it will need to be built
from source using `pip install`.

1. Remove `libwebkit2gtk-3.0-dev` (as the wxWidgets will build with
   this but wxPython expects a built with WebKit 1):

       sudo apt-get remove libwebkit2gtk-3.0-dev

2. Install dependencies:

       sudo apt-get install g++ libjpeg-dev libgtk-3-dev libgl1-mesa-dev \
           libglu1-mesa-dev libgstreamer1.0-dev \
           libgstreamer-plugins-base1.0-dev libgstreamer-plugins-bad1.0-dev \
           python-dev libwebkitgtk-3.0-dev`

3. Install with pip:

       pip install-user -U wxpython

## Upgrading from Releases before 1.96.2

Nodes using the auto-reset feature and __reset magic variable should
be updated to use the new, simpler method by:

1. Replacing expression that update `__name` to set the `__name`
   variable directly. (e.g., `update_hp = __name = "HP [hp] / [total_hp]"`
   should be `__name = "HP [hp] / [total_hp]`).

2. Removing any dice rolls used to trigger name, or icon updates.

3. Using `__roll` in expressions instead of reading `__reset` in a
   reset action (e.g., a damage node could have a
   `__name = "Take [__roll] damage"` expression).

4. Clearing the "Auto reset" checkbox and removing any reset action.

## Upgrading from Releases before 1.95.0

Maps saved in versions prior to 1.95.0 are not compatible and may not
load correctly.

If you manually added miniatures to the `~/.flexirpg/miniatures.xml`
configuration file, these will need to be re-added to the library
using the new menu option (Map -> Add Miniature to Library...).

## Upgrading from Releases before 1.92.0

*Important*

Run the `convert-tree.py` script to convert your game tree _before_
starting this version.  Version 1.92.0 removed all support for game
tree nodes except for flexinodes and will cause all unsupported nodes
to be permanently lost (even if an older version is started).

The convert script has the following limitations:

* "rpg_grid_handler" nodes are not converted.  You will need to
  extract the data manually _before_ running the script and migrate it
  elsewhere (e.g., a campaign/adventure related wiki page).

* The "general" node of d20 character nodes are not converted. As
  above, extract the data manually _before_ running the script.

* Nodes generated from "listctrl" nodes are badly named.  The original
  nodes didn't provide useful names so you may need to fix these up by
  hand.

If the script cannot convert a node it will display an error and the
tree will _not_ be converted.

## New Icons

A set of tree icons for use with 4e D&D powers is now included.  The
following convention for 4e D&D powers is suggested:

* yellow: basic attacks, green: at-will, blue: encounter, red: daily.

* square: standard action; circle: move action; hexagon: minor action;
  triangle: free/immediate action.

* sword: melee; bow: ranged; square: close blast; circle: close burst;
  circle+arrow: area burst; open square: utility/other.
