FlexiRPG
========

FlexiRPG is a rule system agnostic, virtual tabletop for pen and paper
role playing games.

For more information see: https://www.davidvrabel.org.uk/flexirpg/


Requirements
------------

* Python 2.5 or later (http://www.python.org/)
* wxPython 2.8 or later (http://www.wxpython.org/)
* PLY 2.5 or later (http://www.dabeaz.com/ply/)


Launching
---------

* Run flexirpg.py to start the client.
* Run flexirpg-server.py to start the command line server.

-- David Vrabel <david.vrabel@cantab.net>
